<?php
require_once MAIN_APP_PATH."controllers/SiteController.php";

class App {
	private static $_instance = null;

	private function __construct()
	{
		$this->onInit();
	} // end __construct

	public static function run()
	{
		if (self::$_instance) {
			throw new Exception("Application is already running.");
		}

		self::$_instance = new self();
	} // end run

	public function onInit()
	{
		$siteController = $this->_getMainController();

		if ($this->_isAjax()) {
			$siteController->actionAjax();
		} else {
			$siteController->actionIndex();
		}
	} // end onInit

	private function _isAjax()
	{
		return array_key_exists('ajax_action', $_POST);
	} // end _isAjax

	private function _getMainController()
	{
		return new SiteController();
	} // end _getMainController

	private function _hasUrlInRequest()
	{
		return array_key_exists(self::URL_REQUEST_KEY, $_GET)
		       && !empty($_GET[self::URL_REQUEST_KEY]);
	} // end _hasUrlInRequest

	private function __clone() {}
	private function __wakeup() {}
}