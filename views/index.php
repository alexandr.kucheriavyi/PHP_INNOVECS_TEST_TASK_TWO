<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Statistics of transactions</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/site.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

<div class="container">
	<form class="form-signin">
		<h2 class="form-signin-heading">Money Sender</h2>
        <div class="form-group">
            <label class="amountcol-md-12 control-label" for="email" >Email</label>
            <div class="amountcol-md-12">
                <input type="text" name="email" id="email" class="form-control" placeholder="Email" autofocus>
            </div>
        </div>
        <div class="form-group">
            <label class="amountcol-md-12 control-label" for="amount" >Amount</label>
            <div class="amountcol-md-12">
                <input type="text" id="amount" class="form-control" placeholder="Money">
            </div>
        </div>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Send</button>
	</form>

</div> <!-- /container -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"  crossorigin="anonymous"></script>
<script src="/libs/external/jquery.blockUI.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="/assets/js/site.js"></script>
</body>
</html>
