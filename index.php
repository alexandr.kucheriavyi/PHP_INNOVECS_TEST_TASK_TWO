<?php
require_once 'config.php';
require_once 'functions.php';
require_once "App.php";

set_error_handler('onThrowWarningAndNoticeException', E_ALL);

try {
	App::run();
} catch (Exception $e) {
	echo $e->getMessage();
}