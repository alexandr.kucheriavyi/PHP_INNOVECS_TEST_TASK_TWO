var Site = {
	formSelector: 'form',
	formFirstSendStatus: false,
	forms: {
		field: {
			email: '#email',
			amount: '#amount',
		}
	},

	onInit: function () {
		var self = Site;

		jQuery('body').on(
			'submit',
			self.formSelector,
			self.onSubmitTransactionForm
		);

		jQuery('body').on(
			'change',
			self.formSelector + ' input',
			self.onTransactionFormInputChange
		);
	}, // end onInit

	onSubmitTransactionForm: function () {
		event.preventDefault();
		var self = Site;

		self.formFirstSendStatus = true;

		var email = jQuery(self.forms.field.email).val();
		var amount = jQuery(self.forms.field.amount).val();

		var errorStatus = false;

		if (!self.validateEmail(self.forms.field.email)) {
			errorStatus = true;
		}

		if (!self.validateAmount(self.forms.field.amount)) {
			errorStatus = true;
		}

		if (errorStatus) {
			return false;
		}

		self.showLoading(self.formSelector);

		var data = {
			email: email,
			amount: amount,
			ajax_action: true,
		}

		jQuery.post('/', data, self.response, 'json')

	}, // end onSubmitTransactionForm

	onTransactionFormInputChange: function()
	{
		var self = Site;

		if (!self.formFirstSendStatus) {
			return true;
		}

		var selector = this;

		if (jQuery(selector).attr('id') == 'email') {
			self.validateEmail(self.forms.field.email);
		}

		if (jQuery(selector).attr('id') == 'amount') {
			self.validateAmount(self.forms.field.amount);
		}
	}, // end onTransactionFormInputChange

	response: function (response) {
		var self = Site;

		console.log(response);

		if (!response.success) {
			self.displayMessage(response.message, 'alert-danger')
		} else {
			self.displayMessage(response.message, 'alert-success')
		}

		self.hideLoading(self.formSelector);
	}, // end response

	displayMessage: function (message, type) {
		jQuery('.alert').remove();

		var content = '<div class="alert ' + type + '">' + message + '</div>';

		jQuery(content).insertAfter("h2");
	}, // end displayMessage

	hideLoading: function (selector) {
		jQuery(selector).unblock();
	}, // hideLoading

	validateEmail: function (selector) {
		var self = Site;
		var email = jQuery(selector).val();

		if (!email) {
			jQuery(selector).addClass('error');

			self.displayFormFieldError(
				selector,
				'Required field'
			);

			return false;
		} else if (!self.isValidEmail(email)) {
			jQuery(selector).addClass('error');

			self.displayFormFieldError(
				selector,
				'Incorrectly filled'
			);

			return false;
		} else {
			jQuery(selector).removeClass('error');
			self.removeFormFieldError(selector);
			return true;
		}

	}, // end validateEmail

	validateAmount: function (selector) {
		var self = Site;
		var amount = jQuery(selector).val();

		if (!amount) {
			jQuery(selector).addClass('error');

			self.displayFormFieldError(
				selector,
				'Required field'
			);

			return false;
		} else if (!self.isValidAmount(amount)) {
			jQuery(selector).addClass('error');

			self.displayFormFieldError(
				selector,
				'Incorrectly filled'
			);

			return false;
		} else {
			jQuery(selector).removeClass('error');
			self.removeFormFieldError(selector);
			return true;
		}

	}, // end validateAmount

	displayFormFieldError: function (selector, message) {
		var errorSelector = '.field-error-message';

		var parent = jQuery(selector).parent();

		var messageSelector = jQuery(parent).find(errorSelector);

		if (messageSelector.length > 0) {
			jQuery(messageSelector).html(message);
		} else {
			jQuery('<span class="field-error-message">' + message + "</span>").insertAfter(selector)
		}
	}, // end displayFormFieldError

	isValidEmail: function (email) {
		var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return regexp.test(email);
	}, // end isValidEmail

	isValidAmount: function (amount) {
		var regexp = /^[0-9]+(?:\.[0-9]{0,2})?$/;
		return regexp.test(amount);
	}, // end amount

	removeFormFieldError: function (selector) {
		var errorSelector = jQuery(selector).next('.field-error-message');

		errorSelector.remove();
	}, // end removeFormFieldError

	showLoading: function (selector) {
		jQuery(selector).block({
			overlayCSS: {
				backgroundColor: '#fff',
				opacity: 0.8,
				cursor: 'wait'
			},
			message: '',
			blockMsgClass: 'loader-block',
		});
	}, // shshowLoading
}

jQuery(document).ready(function () {
	Site.onInit();
})
