<?php

class ApiComponent
{
	private $_user = 'test@gmail.com';
	private $_pass = 'gHqW1r';
	private $_error;

	const API_URL = 'http://test-task.iteam.biz.ua';
	const API_TRANSACTION_METHOD = 'transaction';

	public function sendTransaction($email, $amount)
	{
		$url = $this->_getRequestUrl($email, $amount);

		$headers = array(
			"POST / HTTP/1.0",
			"Content-type: application/json",
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_USERPWD, $this->_user.':'.$this->_pass);
		$data = curl_exec($ch);

		if (!curl_errno($ch)) {
			return json_decode($data, true);
		} else {
			$this->error = curl_error($ch);
			return false;
		}

		curl_close($ch);
	} // end sendTransaction

	private function _getRequestUrl($email, $amount)
	{
		$vars = array(
			self::API_URL,
			self::API_TRANSACTION_METHOD,
			$email,
			$amount
		);

		return implode('/', $vars);
	} // end _getRequestUrl

	public function getErrorMessage()
	{
		return $this->_error;
	} // end getErrorMessage
}