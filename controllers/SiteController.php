<?php
require_once MAIN_APP_PATH."helpers/ViewHelper.php";
require_once MAIN_APP_PATH."components/ApiComponent.php";

class SiteController
{
	public function actionIndex()
	{
		$viewHelper = new ViewHelper();

		echo $viewHelper->fetch('index.php');
	} // end actionIndex

	public function actionAjax()
	{
		if (!$this->_hasDataInRequest()) {
			$data = array(
				'success' => false,
				'message' => 'Error. Try again Later'
			);

			echo json_encode($data);
			exit();
		}

		$email = $_POST['email'];
		$amount = $_POST['amount'];

		$api = new ApiComponent();
		$response = $api->sendTransaction($email, $amount);

		if (!$response) {
			$data = array(
				'success' => false,
				'message' => $api->getErrorMessage()
			);

		} elseif($this->_isRejectedTransaction($response)) {
			$data = array(
				'success' => false,
				'message' => $response['error_message']
			);
		} else {
			$data = array(
				'success' => true,
				'message' => 'Money sent success. '.
							 'Transaction id is '.$response['id_transaction']
			);
		}

		echo json_encode($data);
		exit();
	} // end actionAjax

	private function _isRejectedTransaction($response)
	{
		return array_key_exists('status', $response)
			   && $response['status'] == 'rejected';
	} // end _isRejectedTransaction

	private function _hasDataInRequest()
	{
		return array_key_exists('email', $_POST)
			   && array_key_exists('amount', $_POST)
			   && !empty($_POST['email'])
			   && !empty($_POST['amount']);
	} // end _hasDataInRequest
}